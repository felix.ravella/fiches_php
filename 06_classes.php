<?php 
// ----- Un peu de théorie -----

// Une classe est comme un moule pour faire des objets. Un objet créé à partir
// d'une classe est appelé instance de cette classe; cette instance peut être
// enregistrée dans une variable pour être appelée, consultée, modifiée ou
// detruite plus tard.

// L'interet d'une classe est de déclarer les variables (ou attributs) et
// fonction (ou methodes) que peuvent avoir les instances de cette classe.
// Ceci permet de se faciliter la vie quand de nombreux objets ont
// le même comportement, mais des valeurs qui peuvent varier


// ----- Allez, un peu de syntaxe pour la declaration de classe -----


// on commence par declarer le bloc de définition de la classe, avec son nom
// (le standard le fait ce fait commencer par une majuscule)
class Eleve {
  // on declare ensuite les attributs. Les attributs, comme les fonctions, peuvent être public (accessible depuis l'exterieur), 
  // private (accessible uniquement depuis une methode à l'interieur de la classe), ou restricted (accès restreint à 
  // l'interieur de la classe et toutes les classes qui en hériteront)
  public $nom;
  private $age;
  public static $nom_delegue; // faux-ami! un attribut static peut etre modifié, mais reste le même entre toutes les instances de la classe

  // --- les fonction __construct() et __destruct() sont présentes mais 'cachées' dans toutes les 
  // classes et sont appellée automatiquement, respectivement à la création de l'instance et 
  // à la destruction.
  // Les redeclarer est optionnel, mais permet d'ajouter des instructions (pour le jargon, on appelle ça 'surcharger' une fonction ) 
  public function __construct(string $valeur_nom="Jean",int $valeur_age=8) { // on peut typer les arguments avant de donner leur nom, et leur mettre des valeur par defaut
    $this->nom = $valeur_nom; // A l'interieur d'une instance, on accede à ses attributs à avec $this ('la variable qui est moi même'),
    $this->age = $valeur_age; // on indique quel attribut on veut avec -> et le nom de l'attribut; $this->age
                              // Attention! Le $ ne va que sur $this. Après le -> , on désigne le nom d'une variable, pas la variable elle même
    if (!isset(self::$nom_delegue)) { // pour les attributs static, on ne s'addesse pas à l'instance elle-même,
      self::$nom_delegue = "Liam";    // mais à toute la classe, la syntaxe est differente; 
    }                                 // on doit utiliser self::$variable   (oui, je sais, c'est pas ouf pour retenir)
    echo "(Creation d'un eleve) Bienvenue, " . $this->nom ."! Le délégué de ta classe est " . self::$nom_delegue . " <br>";
  }

  public function __destruct() {
    echo "Au revoir, " . $this->nom . ". Tu va manquer à " . self::$nom_delegue .".<br>";
  }
  
  // --- les fonctions __set() et __get() (on parle de setter et getter) sont optionnelles, mais si elles sont declarées, elles sont 
  // déclenchées automatiquement quand on essaie d'acceder à une variable, respectivement pour consulter
  // ou modifier. L'interet principal des getter et setter est, s'il sont declarés public, de pouvoir acceder à des 
  // attributs private (en effet, ces setter sont à l'interieur de l'instance et on donc accès au private)
  public function __get($nom_attribut) {
    return $this->$nom_attribut;  
  }
  public function __set($nom_attribut, $valeur) {
    $this->$nom_attribut = $valeur;
  }

  // --- on peut aussi declarer les setter et getter individuellement. le nom est libre, mais il est souvent plus propre de 
  // les appeller setNomDAttribut ou getNomDAttribut (set ou get + le nom en PascalCase)
  // l'interet des getter et setter individuels est de rajouter des verifications ou instructions specifiques
  public function setAge(int $nouvel_age): void { // un setter a besoin d'un argument pour connaitre la valeur à assigner
    if ($nouvel_age >= 1) { // on peut s'assurer qu'on ne modfie l'age que si l'argument à une valeur positive
      $this->age = $nouvel_age;
    }
  }
  public function getAge(): int { // un getteur renvoie une valeur et n'a pas besoin d'argument
    echo "<br>on consulte \$age de " . $this->nom . "<br>"; // on peut rajouter une instruction
    return $this->age;
  }

  // -- en dehors des getter et setter, on peut creer toutes sortes de fonctions
  public function obtenirInfos(): string {
    $str = "getInfos d'éleve :<br>" . $this->nom . " a " . $this->age . " ans, son délégué est " . self::$nom_delegue . ".<br>";
    return $str;
  }


}

// Tout ce pavé au dessus serait plus agréable à manipuler dans son fichier a lui, mais pour 
// la demonstration on va faires le script php juste en dessous

// Creation d'une nouvelle instance avec le mot-clé new
$eleve1 = new Eleve("Louis", 11); // ces arguments sont passés au constructeur
$eleve2 = new Eleve(); // on peut se passer des arguments s'ils ont des valeurs par defaut dans le constructeur

// on accède à un attribut ou une methode avec $instance->nom_propriete
echo "<br> \$eleve1 a ". $eleve1->age . ".<br>"; // attention, comme avec $this dans la classe, on ne met pas le
                                       // $ après la fleche: c'est un nom de proprieté 
echo "<br>--<br>";

echo "usage du setter et getter sur \$eleve2";
$eleve2->setAge(12); 
echo "age de \$eleve2: " . $eleve2->getAge() . ".<br>";

$nouveau = "Pierre";
echo "Le nouveau délégué de la classe s'appellera $nouveau .<br>";
// Syntaxe modification d'une propriété static:   NomDeClasse::$variable
Eleve::$nom_delegue = $nouveau; // //Comme c'est un attribut static, le changement vaut pour toute la classe et ses instances
echo $eleve2->obtenirInfos();

// A la fin d'un script PHP, toute les instances sont detruites.
echo "<br>--<br>";
echo "Fin du script PHP, les instances vont être detruites. La fonction __deconstruct() sera appelée pour chaque instance.";
echo "<br>--<br>";
?>
