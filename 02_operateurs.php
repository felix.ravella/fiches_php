<?php
// ----- I: Les operateurs mathématiques ----- 

//Les plus connus sont +, -, *, / pour addition, soustraction, muliplication,
//division.
$nombre_4 = 2 + 2; //on peut utilise des valeurs brutes ...
$nombre_addition = 8 + $nombre_4;// ou les valeurs contenues dans des variables

echo "8 + $nombre_4 = $nombre_addition"; // imprime "nombre_addition vaut 8 + 4 = 12"
echo "<br><br>";


// --- Bon, merci félix, on peut voir autre chose que du niveau CE2 ?
// Bien sur!

// On peut rendre une variable egale à une modification d'elle même:
$nombre = 4;
$nombre = $nombre + 7;
echo "Le nombre vaut $nombre"; // imprime "Le nombre vaut 11"
echo "<br>";

// Probleme: c'est long, et on se répete: $nombre = $nombre + ...
// Pour y remedier, on a les operateurs "rapides": +=, -=, *=, /=
// ces operateurs modifient la variable en lui appliquant l'operation par
// la valeur placée après
$autre_nombre = 40;
$autre_nombre /= 10; // signifie: $nouveau_nombre = resultat de ($nouveau_nombre / 10)
echo "autre_nombre vaut $autre_nombre"; // imprime "autre_nombre vaut 4"
echo "<br><br>";

$autre_nombre *= 2;
echo "autre_nombre vaut maintenant $autre_nombre"; // imprime autre_nombre vaut 8"
echo "<br><br>";

// Note: il existe une version raccourcie de $variable += 1: $variable++
//                                     et de $variable -= 1: $variable--


// ----- II Quelques autres operateurs ----- 

// La concatenation '.' permet d'accoler deux strings ensembles:
$string_1 = "Les vampires ";
echo $string_1 . "c'est mieux"; // Imprime "Les vampires c'est mieux"
echo "<br><br>";

// ---

// Le modulo '%' permet d'obtenir le reste d'une division entière:
echo "8 % 3 = " . 8 % 3; // imprime "8 % 3 = 2"
echo "<br><br>";

// ---


// L'operateur ternaire '() ? :'
// Cet operateur doit contenir une affirmation entre parenthese.
// Après le '?', le symbole ':' sépare deux options; on utilise la première si
// l'affirmation est vraie, la seconde si elle est fausse.
$age = 18;
$prix_cinema = ($age > 25) ? 4 : 3; 
// Cette ligne equivaut à : rendre $prix_cinema égale à 4 si $age superieure à 25, 
//                                                        sinon le rendre egale à 3

// On peut de plus utiliser l'operateur ternaire avec un operateur rapide:
$qi_felix = 130;
$interlocuteur = "Pierre";
$qi_felix -= ($interlocuteur == "Sarah") ? 50 : 0;
echo "Félix parle avec $interlocuteur, son QI est à $qi_felix.<br>";

$interlocuteur = "Sarah";
$qi_felix -= ($interlocuteur == "Sarah") ? 50 : 0;
echo "Félix parle avec $interlocuteur, son QI est à $qi_felix";
//Cette ligne equivaut à : retrancher 50 à $qi_felix si l'interlocuteur egal "Sarah" 
//                                                   sinon retranche 0    */


?>
