<?php 
// -- Déclarer une variable:
$variableMouette = "Mouette!";  //penser au symbole $ au début
$autre_variable1 = 14;

// -- Imprimer du texte dans la page HTML:
echo("SoLeIl!");
echo $variableMouette;  //Les parenthèses sont optionnelles
echo "<br><div>Je m'infiiiiltre sous vos ongles!<div>";

// -- Interpoler le contenu d'une variable dans un string:
$prenom = "Félix";
$age = 6;
echo "Je m'appelle $prenom, j'ai $age ans "; // Ne marche que si le string est délimité par des "

// -- Imprimer un caractère en dur par symbole \
echo "\$age vaut 6 !"; // $age ne sera pas interpolée car le $ sera ecrite en dure,
                       // plutot que d'être reconnu comme le début d'une variable 
echo "<br> <div style=\"color: red;\">Du rouge<div><br>";// Les \ permet aux " juste après 
                                                         //de ne pas fermer le string.

// -- Operateurs rapides:
$age += 10; // pareil que $age = age + 8, $age vaut maintenant 16;
$age /= 4; // pareil que $age = $age / 4, $age vaut maintenant 4;

// -- Operateur ternaire '() ? :'
$prenom = "Sarah";
$menu = ($prenom == "Sarah") ? "canard" : "autre chose"; // (condition) ? siOui : siNon;

// -- Concatenation de strings par symbole '.' :
$texte = "j'aime le bacon";
$texte_enrichi = $texte . " et les crepes"; // le . joue un peu un role de + avec les strings;

// -- Modulo par symbole '%'
$reste_division = 8 % 3; // 8 = (2 x 3) + 2, $reste_division vaut 2;

// -- Boucle for: --
    // avec declaration et initialisation du compteur:
for ($i = 0; $i < 10; $i++) { // (compteur; condition de repetition; modif à chaque boucle)
 echo "\$i vaut $i";
}

    // avec une variable precedement définie comme compteur:
for ($age; $age > $reste_division; $age -= 5) {
  $texte_enrichi .= " parce que c'est bon!";
}

// -- Boucle foreach:
$semaine = ["lundi", "mardi", "mercredi", "laflemme"];
foreach ($semaine as $jour) { // signifie "pour chaque element de $semaine, qu'on apellera $jour:"
  echo "Aujourd'hui c'est $jour";
  echo "<br>";
}

$liste = ["Rhone" =>85, "Ain" => 24, "Bretagne" => 452];
foreach ($liste as $departemetn=>$alcoolisme) { // signifie pour chaque paire de clé valeur, qu'on appellera respectivement $prenom et $cuteness
  echo "Il y a $alcoolisme bars à ";
  echo "<br>";
}

// -- Creation de classe
class Eleve {
  public $nom;                // public: accessible de l'exterieur
  private $age;               // private: accessible uniquement à l'interieur
  public static $effectifs; // static: commun à toutes les instances

  // eventuellement surcharge de construct pour assigner des valeurs et
  // instructions lors de la creation
  public function __construct(string $valeur_nom="Jean",int $valeur_age=8) {     
    // acceder à un de ses attributs: $this->nomDAttribut
    $this->nom = $valeur_nom;
    $this->age = $valeur_age;
    // acceder à un attributs static: self::$nomDAttribut
    self::$effectifs += 1;
  }

  public function __destruct() {
    self::$effectifs -= 1;
  }

  //getter et setter individuel pour acceder aux attribut private de l'exterieur
  public function setAge(int $nouvel_age): void {
    $this->age = $nouvel_age;
  }
  public function getAge(): int { // un getteur renvoie une valeur et n'a pas besoin d'argument
    return $this->age;
  }

  // autres fonctions eventuelles
  public function manger($aliment) {
    echo $this->nom . "mange du $repas";
  }
}

// -- Usage des classes
    // creation d'instance
$eleve = new Eleve("Sam", 16); // new appelle le constructeur. Si aucun paramètre n'est 
$autre_eleve = new Eleve();    // donné, le constructeur utilisera les valeurs par défaut       
                               // qu'on a défini à sa declaration dans la classe                

    // acceder à un attribut ou un attribut d'une instance
echo $autre_eleve->nom;
$ $autre_eleve->setAge(14);


?>
