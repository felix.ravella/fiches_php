<?php
// -- Un peu de théorie ---

// les boucles for permettent de repeter des opérations un nombre défini de fois.

// Note: 'defini' ne signifie pas 'connu lors de l'ecriture du code'
// un string contenant le prenom d'un utilisateur est défini (au moment
// ou on l'a, on connait sa longueur) mais inconnu ( on ne peut pas parier sur
// sa longueur au moment de l'ecriture )


// La boucle For permet donc d'avoir un code plus adaptable selon les aléas et 
// reactifs aux valeurs des variables.

/* Syntaxe: en PHP, la boucle For se déclare, schematiquement, comme suit:
for ( compteur; condition de repetition; decalage) { 
  instructions à chaque iteration
} 
-compteur sera la variable qui portera le chiffre relative au nombre de boucles
-decalage sera la modification à apporter au compteur à la fin de chaque boucle
-les paramètres doivent être declarés entre parenthèse, séparés par des points-virgules.
-le bloc d'instructions doit etre entourées d'accolades.
-pas de point virgule à la fin du bloc, ni entre les paramètres et le debut du bloc
*/

// exemple concret:
$heure_actuelle = 0;
$nombre_boucles = 5;

for ( $heure_actuelle; $heure_actuelle < $nombre_boucles; $heure_actuelle++) {
  echo "Ding! Il est $heure_actuelle heures!";
  echo "<br>";
}
echo "---<br>";
// la boucle ci-dessous fonctionne ainsi:
// 1) le compteur (parametre 1) a sa valeur définie (en l'ocurrence avant la boucle)
// 2) si la condition  de repetition (parametre 2) est vraie, on effectue les instructions
// 3) puis on modifie le compteur avec le decalage (+1, en l'ocurrence)
// 4) puis on recommence à l'etape 2


// Il est egalement possible de declarer un compteur specialement pour les
// besoins de la boucle
// De plus, on peut aussi faire partir notre compteur d'une autre valeur que 0, et
// avoir un décalage différent de +1
for ( $oeufs = 3; $oeufs < 16 ; $oeufs += 6) {
  echo "Nous avons $oeufs oeufs";
  echo "<br>";
}
echo "---<br>";
// dans l'exemple ci-dessus, le compteur est declaré en même temps qu'on déclare
// la variables $oeufs, avec une valeur de départ de 3.
// Cette variables $oeufs n'existe que dans la boucle et ne pourra pas être
// appelée ailleurs, a moins d'être re-declarée

// --------------
// Variante de for: foreach
// plutot que de boucler avec un compteur, foreach va iterer sur chaque
// element d'une liste.

/* Syntaxe: en PHP, la boucle For se déclare, schematiquement, comme suit:
 
for ( liste as alias ) { 
  instructions à chaque iteration
} 

// la source doit être un variable contenant un tableau
// l'alias est un nom de variable (avec un $ au debut comme toujours), qui sera
// utilisé pour avoir la valeur de l'element actuel de la boucle
// comme pour for, le bloc d'instructions doit etre entourées d'accolades.
// pas de point virgule à la fin du bloc, ni entre les paramètres et le debut
// du bloc
*/

// exemple concret:
$semaine = ["lundi", "mardi", "mercredi", "laflemme"];
foreach ($semaine as $jour) { // signifie "pour chaque element de $semaine, qu'on apellera $jour:"
  echo "Aujourd'hui c'est $jour";
  echo "<br>";
}
echo "---<br>";

// dans le cas des tableaux associatifs, qui font des paires clé=>valeur, on
// peut utiliser foreach pour avoir la clé et la valeur à chaque iteration
$liste = ["Marine" => 4, "Valerie" => 15, "Sarah" => 999];
foreach ($liste as $prenom=>$cuteness) { // signifie pour chaque paire de clé valeur, qu'on appellera respectivement $prenom et $cuteness
  echo "$prenom a une cuteness de $cuteness.";
  echo "<br>";
}
echo "---<br>";


/* --- Des exercices, peut être? ---

A.1) Ecrire une boucle for pour afficher tous les chiffes de 5 à 9
  2) Ecrire une boucle for pour afficher tous les nombres paires de 10 à 0 (decroissant)

B.1)
Quelle string donner à la fonction echo pour afficher un bouton? Stocker ce string dans
une variable
*/
?>
