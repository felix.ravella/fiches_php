<?php 
// ----- Un peu de théorie -----

// Lors de la creation d'une classe, on peut lui faire hériter des
// propriétés d'une autre classe (qu' on appellera la classe 'mère' ou 'parente' )
// La classe qui hérite (ou classe fille) aura tous les attributs et methodes de
// la classe mère.

// Note: pour rappel, une proprité private n'est pas transmise


// ----- Exemple concret -----

// Classe mère
class Animal {
  public $poids;

  public function __construct(int $nouveau_poids = 60){
    $this->poids = $nouveau_poids; //rappel: acceder à une propriété depuis l'interieur de 
                                   // la classe se fait avec $this->nomPropriete, $ sur le 'this'
                                   // mais pas le 'nomPropriete'
  }
  public function deplacer() {
    echo "Je marche!<br>";
  }
}

// Classe fille
class Chien extends Animal { // le mot clé extends indique que Chien hérite de Animal
  public $race;
}

$nouveau_chien = new Chien();
$nouveau_chien->race = "Dobberman";

// Chien hérite de Animal, elle en a donc les attributs et methodes, en plus des siennes.
// on a donc le droit d'appeler deplacer() qui est heritée de Animal
$nouveau_chien->deplacer(); // imprime "je marche!" 

// De même que poids, qui a été initialisé car on a appelé le constructeur de
// Animal en creant un objet d'une classe qui en hérite
echo "Poids chien: " . $nouveau_chien->poids; 

echo "<br>---<br>";


// ----- Remplacer une methode existante dans la classe mère -----

// Il est possible de remplacer une propriété d'une classe mere en la
// redeclarant dans la classe fille
class Serpent extends Animal {
  public function deplacer() {
    echo "TsssssSSSsss! Je rampe!<br>";
  }
}

$nagini = new Serpent();  // ici, le constructeur de Animal sera appelé car il n'a pas été
                          // remplacé dans la classe Serpent
                          
$nagini->deplacer();  //  imprime "TsssssSSSsss! Je rampe!" car deplacer() a été redeclarée dans 
                      // la classe fille et remplace donc celle de la classe mère

echo "<br>---<br>";


// ----- Niveau superieur! On garde le meilleur des deux mondes! -----

// On a parfois besoin qu'une classe fille fasse ce que fait sa classe mère,
// mais qu'elle fasse egalement un peu plus. On a parfois besoin d'etendre les
// fonctionnalité d'une methode, mais pas de la remplacer completement.
// Pour ce faire, on peut faire qu'une methode redeclarer puisse appeler la
// version d'elle-meme qu'avait la classe mère

// Syntaxe: on accède à la propriété d'une classe parente avec parent::nomProprieté
// ça rappelle un peu self::nomPropriete (qu'on utilise pour acceder à une
// proprité static de la classe actuelle plutot qu'une public d'une classe mère)

class Oiseau extends Animal {
  public function deplacer() {
    parent::deplacer(); // ici, on appelle deplacer() d'une classe parente (Animal en l'occurence) 
    echo "Mais je vole aussi!"; // et ici, les fonctionnalité qu'on désire pour Oiseau
    // avec ceci, appeler deplacer() sur une instance de Oiseau fera les
    // fonctionnalités des deplacer() de la classe Animal, et aussi les
    // fonctionalités spécifiques qu'on aura rajouté
  }
}

$perruche = new Oiseau();
$perruche->deplacer(); // imprime Je marche!
                       //         Mais je vole aussi!

?>
