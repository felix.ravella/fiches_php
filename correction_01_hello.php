<?php
// A.1)
$mots = "Lorem ipsum";
// A.2)
echo "<div>$mots</div>"; // bien penser à utiliser des " et non des ' pour l'interpolation

//B.1)
$color = "red";
//B.2)
$styles = "style=\"color: $color;margin-left: 20px;\" "; // bien utiliser des \ pour garder les "
                                                         // a l'interieur du string
//B.3)
echo "<p $styles >$mots</p>";
?>
