<?php
/* En SQL, un JOIN permet de recuperer des données situées sur plusieurs
 tables en même temps, en les *joignant* selon une ou des conditions.

Supposont une table users
|__id__|___nom___|
|  1   |  Jean   |
|  2   |  Pierre |

et une table articles
|__id__|___titre___|__user-id__|
|  1   |  Mouette  |    1      |
|  2   |  Debout!  |    1      |
|  3   |  Youpi    |    2      |

Recuperer les données d'un user, on sait faire:
SELECT * FROM users;

Recuperer juste le tire des articles, on sait faire:
SELECT titre FROM articles;

Mais comment recuperer, en une requete, les users ET tous les titres d'articles qu'ils ont ecrit?

Pour ce faire, on utilise un JOIN.
Il y en a plusieurs sortes, mais INNER JOIN est celui qui nous interesse: il recupère l'intersection des tables
(la somme des données qu'on desire) uniquement la ou une condition se verifie

Syntaxe:
SELECT users.id, users.nom, articles.titre  // partie SELECT: on declare les colonnes des tables que l'on veut garder
FROM users                                  // partie FROM: on declare la table qui sera la source (premiere partie de la jointure)
INNER JOIN articles                         // partie INNER JOIN: on declare sur quelle table verifier l'intersection (deuxieme partie de la jointure)
ON users.id = articles.user-id;             // partie ON: on defini la condition sur laquelle on  associe les données des tables

Cette requete nous renverra:
|__id__|___nom___|___titre___|
|   1  |   Jean  |  Mouette  |
|   1  |   Jean  |  Debout!  |
|   2  |  Pierre |   Youpi   |

*/

//Bien sur, ces commandes SQL sont parfaitement compatibles avec un objet PDO:

$instance_pdo = new PDO("mysql:host=localhost;dbname=maBaseDeDonnee;charset=utf88", "root", "");
$requete = $instance_pdo->query("SELECT users.id, users.nom, articles.titre FROM users INNER JOIN articles ON users.id = articles.user-id");

?>
